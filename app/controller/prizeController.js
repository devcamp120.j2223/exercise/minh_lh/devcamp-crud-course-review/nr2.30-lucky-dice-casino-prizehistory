//import model
const prizeModel = require('../model/prizeModel');
// khai báo mogoose
const mongoose = require('mongoose');

// create new prize
const createNewPrize = (request, response) => {
    //b1 lấy dữ liệu
    let body = request.body;
    //b2 validate dữ liệu
    if (!body.name) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "name is not valid"
        })
    }
    // b3 create new prize
    let prizeNew = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description,
    }
    prizeModel.create(prizeNew, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Create prize success",
                data: data
            })
        }
    })
}

// get all prize
const getAllPrize = (request, response) => {
    // B1: thu thập dữ liệu
    // B2: validate dữ liệu
    // B3: thao tác với cơ sở dữ liệu
    prizeModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: get all prize success",
                data: data
            })
        }
    })
}
// get prize by id
const getPrizeById = (request, response) => {
    //B1: thu thập dữ liệu
    let prizeId = request.params.prizeId;
    //B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(prizeId))) {
        return response.status(500).json({
            status: "Error 500: Internal sever Error",
            message: error.message
        })
    }
    //B3: thao tác với cơ sở dữ liệu
    prizeModel.findById(prizeId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: get all prize success",
                data: data
            })
        }
    })
}
// update prize
const getUpdatePrize = (request, response) => {
    //B1: thu thập dữ liệu
    let prizeId = request.params.prizeId;
    let body = request.body;
    //B2: validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(prizeId))) {
        return response.status(500).json({
            status: "Error 500: Internal sever Error",
            message: error.message
        })
    }
    if (!body.name) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "name is not valid"
        })
    }
    let prizeUpdate = {
        name: body.name,
        description: body.description,
    }
    //B3: thao tác với cơ sở dữ liệu
    prizeModel.findOneAndUpdate(prizeId, prizeUpdate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update prize History success",
                data: data
            })
        }
    })
}
// delete prize by id
const deletePrizeById = (request, response) => {
    //B1: thu thập dữ liệu
    let prizeId = request.params.prizeId;
    //B2: validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(prizeId))) {
        return response.status(500).json({
            status: "Error 500: Internal sever Error",
            message: error.message
        })
    }
    //B3: thao tác với cơ sở dữ liệu
    prizeModel.findOneAndDelete(prizeId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Success: Delete prize History succesa"
            })
        }
    })
}

//exports
module.exports = {
    createNewPrize, getAllPrize, getPrizeById, getUpdatePrize, deletePrizeById
}