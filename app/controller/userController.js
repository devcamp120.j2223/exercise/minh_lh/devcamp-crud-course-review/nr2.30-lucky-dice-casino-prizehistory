// IMPORT dice history model  vào controller
const userModel = require('../model/userModel');

//Khai báo thư viện mongoose
const mongoose = require('mongoose');


//CREATE A USER
const createUser = (request, response) => {

    //B1: thu thập dữ liệu
    let bodyRequest = request.body;
    console.log(bodyRequest)

    //B2: Validate dữ liệu

    if (!bodyRequest.username) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User name is require"
        })
    }

    if (!bodyRequest.firstname) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "First name is require"
        })
    }

    if (!bodyRequest.lastname) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Last name is require"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    let createUser = {
        _id: mongoose.Types.ObjectId(),
        username: bodyRequest.username,
        firstname: bodyRequest.firstname,
        lastname: bodyRequest.lastname
    }

    userModel.create(createUser, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Create dice history success",
                data: data
            })
        }
    })
}


// LẤY TẤT CẢ USER
const getAllUser = (request, response) => {
    //B1: thu thập dữ liệu
    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    userModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all user success",
                data: data
            })
        }
    })
}


// LẤY USER THEO ID
const getUserById = (request, response) => {
    //B1: thu thập dữ liệu
    let userId = request.params.userId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "User ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    userModel.findById(userId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get user by id success",
                data: data
            })
        }
    })
}

//UPDATE A USER
const updateUserById = (request, response) => {
    //B1: thu thập dữ liệu
    let userId = request.params.userId;
    let bodyRequest = request.body;
    console.log(bodyRequest);

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "User ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    let userUpdate = {
        username: bodyRequest.username,
        firstname: bodyRequest.firstname,
        lastname: bodyRequest.lastname
    }
    userModel.findByIdAndUpdate(userId, userUpdate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update user success",
                data: data
            })
        }
    })
}

// DELETE A USER
const deleteUserById = (request, response) => {
    //B1: thu thập dữ liệu
    let userId = request.params.userId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Drink Id is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    userModel.findByIdAndDelete(userId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Delete user success"
            })
        }
    })
}

// EXPORT
module.exports = { createUser, getAllUser, getUserById, updateUserById, deleteUserById };
